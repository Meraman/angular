var myApp = angular.module("myModule",[]);
myApp.controller("myController", function($scope, $http, $log){
    var successCallback = function (response){
        $scope.data = response.data;
        $log.info(response);
    };
    var errorCallback = function(reason){
        $scope.error = reason.data;
    };
    $http({
        method: "GET",
        url: "http://localhost/dbDemo/fetchDB.php"
    }).then(successCallback, errorCallback);
    $scope.myTable = 'myTable.html';
});