var myApp=angular.module("myModule",[]);
myApp.controller("myController",function($scope){
    var students=[
        {firstName:"Akhil",lastName:"Viroja",semester:5,likes:0,dislikes:0,path:"akhil.png"},
        {firstName:"Shrutavik",lastName:"Kahnapara",semester:3,likes:0,dislikes:0,path:"shrutavik.png"},
        {firstName:"chink",lastName:"neet",semester:5,likes:0,dislikes:0,path:"plk.png"},
    ];
    $scope.students=students;
    $scope.incrementLikes=function(student){
        student.likes++;
    };
    $scope.incrementDisLikes=function(student){
        student.dislikes++;
    };
});
