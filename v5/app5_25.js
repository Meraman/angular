var sortingAppModule=angular.module('sortingApp',[]);
sortingAppModule.controller('sortingControl',function($scope){
    var dataSource = [
        { "zone": "SOUTH","state": "MAHARASHTRA","city": "SANGAMNER","branchName":"SHRIRAMPUR"},
        { "zone": "NORTH","state": "UTTAR PRADESH","city": "AGRA","branchName":"AGRA"},
        { "zone": "NORTH","state": "UTTARAKHAND","city": "DEHRADUN","branchName":"YAMUNANAGAR"},
        { "zone": "SOUTH","state": "TELANGANA","city": "HYDERABAD","branchName":"WARNGAL"},
        { "zone": "NORTH","state": "GUJARAT","city": "AHMEDABAD","branchName":"AHEMDABAD REGIONAL OFFICE"},
        { "zone": "SOUTH","state": "KARNATAKA","city": "BELGAUM","branchName":"BELGAUM" },
        { "zone": "SOUTH","state": "MAHARASHTRA","city": "SANGAMNER","branchName":"URBAN" },
        { "zone": "SOUTH","state": "MAHARASHTRA","city": "SOLAPUR","branchName":"BARSHI" },
        { "zone": "SOUTH","state": "TAMILNADU","city": "MADURAI","branchName":"VIRUDHUNAGAR"},
        { "zone": "SOUTH","state": "KARNATAKA","city": "DAVANGERE","branchName":"SHIMOGA"},
        { "zone": "SOUTH","state": "MAHARASHTRA","city": "PUNE","branchName":"THANE"},
        { "zone": "SOUTH","state": "TAMILNADU","city": "MADURAI","branchName":"THENI" },
        { "zone": "SOUTH","state": "TAMILNADU","city": "COIMBATORE","branchName":"UDUMALPETH"},
        { "zone": "NORTH","state": "GUJARAT","city": "SURAT","branchName": "VYARA"},
        { "zone": "SOUTH","state": "MAHARASHTRA","city": "NAGPUR","branchName":"WARDHA"},
    ];
    $scope.data=dataSource;
    $scope.sortColumn="+zone";
});
