// var myApp = angular.module("myModule", []);
// var myController = function($scope){
//     var message = "Hello World";
//     $scope.message = message;
// }
// myApp.controller("myController", myController);

//or p3
// var myApp = angular.module("myModule", []);
// myApp.controller("myController", function($scope){
//     var message = "Hello World";
//     $scope.message = message;
// });

//or p4

var myApp = angular.module("myModule", [])
    .controller("myController", function($scope){
    var message = "Hello World";
    $scope.message = message;
});