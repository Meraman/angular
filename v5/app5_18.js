var myApp = angular.module("myModule", []);
var myController = function ($scope)
{
    $scope.Movies = [
        { MovieType: "Holywood Movies", likes: 0, dislikes: 0 },
        { MovieType: "Bolywood Movies", likes: 0, dislikes: 0 },
        { MovieType: "Tolywood Movies", likes: 0, dislikes: 0 },
        { MovieType: "German Movies", likes: 0, dislikes: 0 }
    ];
    $scope.incrementLikes=function(Mov)
    { Mov.likes++ }
    $scope.incrementDisLikes = function (Mov)
    { Mov.dislikes++ }
};
myApp.controller("myController", myController);